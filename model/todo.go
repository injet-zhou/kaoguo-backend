package model

type Todo struct {
	ID           uint32 `gorm:"primarykey" json:"id"`
	Content      string `gorm:"varchar(100)" json:"content"`
	CreateTime   int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	ShouldFinish int64  `json:"should_finish"`
	Finish       bool   `json:"finish"`
	UserId       uint32
}
