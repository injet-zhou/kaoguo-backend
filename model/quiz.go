package model

type Quiz struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Username   string `gorm:"varchar(50)" json:"username"`
	Answer     string `gorm:"varchar(255)"`
}
