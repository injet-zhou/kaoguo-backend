package model

type Moment struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Username   string `gorm:"varchar(50)" json:"username"`
	Title      string `gorm:"varchar(300)" json:"title"`
	Content    string `gorm:"varchar(500)" json:"content"`
}

type Comment struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Username   string `gorm:"varchar(50)" json:"username"`
	Content    string `gorm:"varchar(300)" json:"content"`
	ToUser     string `gorm:"varchar(50)"`
}

type Like struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Username   string `gorm:"varchar(50)" json:"username"`
	MomentId   uint32
	ToUser     string `gorm:"varchar(50)"`
}

type LikeMessage struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	LastUpdate int64  `json:"last_update"`
	LikeCount  uint32
	Username   string `gorm:"varchar(50)" json:"username"`
	ToUser     string `gorm:"varchar(50)"`
}
