package model

type User struct {
	UserId     uint32 `gorm:"primarykey" json:"user_id"`
	Username   string `gorm:"varchar(50)" json:"username"`
	Password   string `gorm:"varchar(50)" json:"password"`
	Phone      string `gorm:"varchar(50)" json:"phone"`
	Avatar     string `gorm:"varchar(200)" json:"avatar"`
	Year       int64  `json:"year"`
	Target     string `gorm:"varchar(50)" json:"target"`
	Todos      []Todo `gorm:"foreignkey:UserId"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Token      string `gorm:"varchar(200)"`
	NickName   string `gorm:"varchar(50)"`
}

type Record struct {
	ID         uint32 `gorm:"primarykey" json:"id"`
	CreateTime int64  `gorm:"autoCreateTime:nano" json:"create_time"`
	Content    string `gorm:"varchar(100)" json:"content"`
	Username   string `gorm:"varchar(50)" json:"username"`
}
