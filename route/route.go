package route

import (
	"github.com/gin-gonic/gin"
	"os"
	u "sinkcode.cn/exam/handler/user"
	"sinkcode.cn/exam/middleware"
)

var runMode string

func init() {
	runMode = gin.DebugMode
	if os.Getenv("PROGRAM_ENV") == "pro" {
		runMode = gin.ReleaseMode
	}
}

func InitRoute() *gin.Engine {
	r := gin.Default()
	r.Use(middleware.CORSMiddleware())
	user := r.Group("/user")
	{
		user.POST("login", u.LoginHandler)
		user.POST("register", u.RegisterHandler)
	}
	return r
}
