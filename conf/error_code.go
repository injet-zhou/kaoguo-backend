package conf

const (
	Success          = 0
	DeserializeError = 1001
	FieldEmptyError  = 1002
)

var ErrorMsgMap = map[int32]string{
	Success:          "成功",
	DeserializeError: "反序列化失败",
	FieldEmptyError:  "有空表单项",
}
