package user

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"sinkcode.cn/exam/conf"
	pb "sinkcode.cn/exam/proto"
)

func LoginHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hello World",
	})
}

func RegisterHandler(c *gin.Context) {
	var reqData pb.RegisterRequest
	if err := c.Bind(&reqData); err != nil {
		c.ProtoBuf(http.StatusOK, &pb.RegisterResponse{
			ErrorCode: conf.DeserializeError,
			ErrorMsg:  conf.ErrorMsgMap[conf.DeserializeError],
		})
		return
	}
	username := reqData.Username
	phone := reqData.Phone
	password := reqData.Password
	if username == "" || phone == "" || password == "" {
		c.ProtoBuf(http.StatusOK, &pb.RegisterResponse{
			ErrorCode: conf.FieldEmptyError,
			ErrorMsg:  conf.ErrorMsgMap[conf.FieldEmptyError],
		})
		return
	}
	c.ProtoBuf(http.StatusOK, &pb.RegisterResponse{
		ErrorCode: conf.Success,
		ErrorMsg:  "Hello",
	})
}
