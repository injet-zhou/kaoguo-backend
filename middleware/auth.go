package middleware

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"sinkcode.cn/exam/lib"
	"sinkcode.cn/exam/model"
	"sinkcode.cn/exam/util"
)

const (
	TokenHeader     = "Authorization"
)

func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.Request.Header.Get(TokenHeader)


		var condition map[string]interface{}
		if token == "" {
			c.AbortWithStatus(401)
			return
		} else if token != "" {
			claims, err := util.ParseJwtToken(token)
			if err != nil {
				c.AbortWithStatus(401)
				return
			}
			condition = map[string]interface{}{
				"id": claims.UserId,
			}
		}

		conn := lib.DBConn()
		var user model.User
		if err := conn.Where(condition).First(&user).Error; errors.Is(err, gorm.ErrRecordNotFound) {

			c.AbortWithStatus(401)
			return
		}

		c.Keys = map[string]interface{}{"user": user}
		c.Next()
		return
	}
}

