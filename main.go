package main

import (
	"fmt"
	"sinkcode.cn/exam/lib"
	"sinkcode.cn/exam/model"
	"sinkcode.cn/exam/route"
)

func main() {
	db := lib.DBConn()
	db.AutoMigrate(&model.User{},&model.Todo{},&model.Record{})
	r := route.InitRoute()
	if err := r.Run(); err != nil {
		fmt.Printf("Failed to start server, err: %s",err.Error())
	}
}
