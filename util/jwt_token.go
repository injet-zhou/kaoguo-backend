package util

import (
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/goinggo/mapstructure"
)

var (
	JwtSecretKey = []byte("kaoguo")
)

type JWTToken struct {
	UserId    uint32
	jwt.StandardClaims
}

func GenJwtToken(claims jwt.Claims) (tokenString string, err error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(JwtSecretKey)
}

func ParseJwtToken(tokenString string) (*JWTToken, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return JwtSecretKey, nil
	})
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		var jwtToken JWTToken
		if err := mapstructure.Decode(claims, &jwtToken); err != nil {
			return nil, err
		}
		return &jwtToken, nil
	} else {
		fmt.Println("token ->", token)
		return nil, errors.New("jwt token not valid")
	}
}

