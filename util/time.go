package util

import (
	"time"
)

func NowDateNano() int64 {
	now := time.Now()
	location, _ := time.LoadLocation("Asia/Shanghai")
	today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, location)
	//dur := now.UnixNano() - today.UnixNano()
	//fmt.Println(time.ParseDuration(fmt.Sprintf("%dns",dur)))
	return today.UnixNano()
}
